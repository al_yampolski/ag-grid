import { Component } from '@angular/core';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-image-formatter',
  templateUrl: './image-formatter.component.html',
  styleUrls: ['./image-formatter.component.scss'],
})
export class ImageFormatterComponent {
  params: ICellRendererParams;

  agInit(params: ICellRendererParams) {
    this.params = params;
  }
}
