// import { TestBed, async, ComponentFixture, tick, flush } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { AgGridModule } from 'ag-grid-angular';
// import { ImageFormatterComponent } from './components/image-formatter/image-formatter.component';
// import { ModeToolComponent } from './components/mode-tool/mode-tool.component';
// import { TotalToolComponent } from './components/total-tool/total-tool.component';
// import { SelectedToolComponent } from './components/selected-tool/selected-tool.component';
// import { SelectedCellRenderComponent } from './components/selected-cell-render/selected-cell-render.component';
// import { HeaderCustomComponent } from './header-custom/header-custom.component';
// import { fakeAsync } from '@angular/core/testing';

// import { CommonService } from '@services/common.service';
// import { asyncScheduler as _async, of } from 'rxjs';

// const commonServiceMock = {
//   getFormateData() {
//     const data = rowData;
//     return of(data, _async);
//   },
// };
// const rowData = [
//   {
//     thumbnails: 'https://i.ytimg.com/vi/SDqQY2qMMxw/default.jpg',
//     publishedAt: '31-Oct-2012 05:02',
//     title: 'https://www.youtube.com/watch?v=SDqQY2qMMxw',
//     description: 'This is the holy book of John, known as',
//     select: false,
//   },
//   {
//     thumbnails: 'https://i.ytimg.com/vi/ftDvPvRf37U/default.jpg',
//     publishedAt: '27-Jan-2020 17:31',
//     title: 'https://www.youtube.com/watch?v=ftDvPvRf37U',
//     description:
//       'Baby John',
//     select: false,
//   },
// ];
// describe('AppComponent', () => {
//   let component: AppComponent;
//   let fixture: ComponentFixture<AppComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         FormsModule,
//         AgGridModule.withComponents([
//           ImageFormatterComponent,
//           ModeToolComponent,
//           TotalToolComponent,
//           SelectedToolComponent,
//           SelectedCellRenderComponent,
//           HeaderCustomComponent,
//         ]),
//       ],
//       declarations: [
//         AppComponent,
//         ImageFormatterComponent,
//         ModeToolComponent,
//         TotalToolComponent,
//         SelectedToolComponent,
//         SelectedCellRenderComponent,
//         HeaderCustomComponent,
//       ],
//       providers: [{ provide: CommonService, useValue: commonServiceMock }],
//     }).compileComponents();

//     fixture = TestBed.createComponent(AppComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   }));

//   it('It should create 8 cells', fakeAsync(() => {
//     fixture.detectChanges();
//     const cellElements = fixture.nativeElement.querySelectorAll('.ag-cell-value');
//     expect(cellElements.length).toEqual(8);
//     fixture.destroy();
//     flush();
//   }));

//   it('should have expected column headers', fakeAsync(() => {
//     component.rowData = rowData;
//     fixture.detectChanges();
//     const elm = fixture.nativeElement;
//     const grid = elm.querySelector('ag-grid-angular');
//     const headerCells = grid.querySelectorAll('.ag-header-cell-text');
//     const headerTitles = Array.from(headerCells).map((cell: any) =>
//       cell.textContent.trim()
//     );
//     expect(headerTitles).toEqual(['', 'Published on', 'Video Title', 'Description']);

//     fixture.destroy();
//     flush();
//   }));

// });
