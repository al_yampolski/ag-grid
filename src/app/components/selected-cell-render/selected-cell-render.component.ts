import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  templateUrl: './selected-cell-render.component.html',
  styleUrls: ['./selected-cell-render.component.scss'],
})
export class SelectedCellRenderComponent implements ICellRendererAngularComp {
  params: any;
  sorted: string;
  select = false;

  agInit(params): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }
}
